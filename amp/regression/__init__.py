from ..utilities import ConvergenceOccurred
from scipy.optimize import minimize


class Regressor:

    """Class to manage the regression of a generic model. That is, for a
    given parameter set, calculates the cost function (the difference in
    predicted energies and actual energies across training images), then
    decides how to adjust the parameters to reduce this cost function.
    Global optimization conditioners (e.g., simulated annealing, etc.) can
    be built into this class.

    Parameters
    ----------
    optimizer : str
        The optimizer to use. Any default optimizers present in the
        scipy.optimize.minimize module like 'L-BFGS-B', 'NCG' or 'TNC' can
        used along with basinhopping. Alternatively, any function can
        be supplied which behaves like scipy.optimize.fmin_bfgs.
    optimizer_kwargs : dict
        Optional keywords for the corresponding optimizer.
    lossprime : boolean
        Decides whether or not the regressor needs to be fed in by gradient of
        the loss function as well as the loss function itself.
    """

    def __init__(self, optimizer='BFGS', optimizer_kwargs=None,
                 lossprime=True):
        """optimizer can be specified; it should behave like a
        scipy.optimize optimizer. That is, it should take as its first two
        arguments the function to be optimized and the initial guess of the
        optimal paramters. Additional keyword arguments can be fed through
        the optimizer_kwargs dictionary."""
        user_kwargs = optimizer_kwargs
        optimizer_kwargs = {}
        if optimizer == 'BFGS':
            optimizer_kwargs = {'gtol': 1e-15, }
        elif optimizer == 'L-BFGS-B':
            optimizer_kwargs = {'gtol': 1e-15,
                                'ftol': 0.,
                                'maxfun': 1000000,
                                'maxiter': 1000000}
            import scipy
            from distutils.version import StrictVersion
            if StrictVersion(scipy.__version__) >= StrictVersion('0.17.0'):
                optimizer_kwargs['maxls'] = 2000
        elif optimizer == 'TNC':
            optimizer_kwargs = {'ftol': 0.,
                                'xtol': 0.}
        elif optimizer == 'NCG':
            optimizer_kwargs = {'avextol': 1e-15, }
        elif optimizer == 'Nelder-Mead':
            lossprime = False
            optimizer_kwargs = {'fatol': 0.,
                                'xatol': 0.,
                                'maxiter': 1000000}
        elif optimizer == 'Powell':
            lossprime = False
            optimizer_kwargs = {'ftol': 0.,
                                'xtol': 0.,
                                'maxiter': 1000000}
        elif optimizer == 'CG':
            optimizer_kwargs = {'gtol': 1e-15,
                                'maxiter': 1000000}
        elif optimizer == 'Newton-CG':
            optimizer_kwargs = {'xtol': 0.,
                                'maxiter': 1000000}
        elif optimizer == 'COBYLA':
            lossprime = False
            optimizer_kwargs = {'maxiter': 1000}
        elif optimizer == 'SLSQP':
            optimizer_kwargs = {'ftol': 0.,
                                'maxiter': 100}
        # elif optimizer == 'dogleg':
        # elif optimizer == 'trust-ncg':
        # elif optimizer == 'trust-krylov':
        # elif optimizer == 'trust-exact':
        elif optimizer == 'basinhopping':
            from scipy.optimize import basinhopping as method
            lossprime = False
            optimizer_kwargs = {}
            self.method = method
        if user_kwargs:
            optimizer_kwargs.update(user_kwargs)
        self.minimize = minimize
        self.optimizer_kwargs = optimizer_kwargs
        self.lossprime = lossprime
        self.optimizer = optimizer

    def regress(self, model, log):
        """Performs the regression. Calls model.get_loss,
        which should return the current value of the loss function
        until convergence has been reached, at which point it should
        raise a amp.utilities.ConvergenceException.

        Parameters
        ----------
        model : object
            Class representing the regression model.
        log : str
            Name of script to log progress.
        """
        log('Starting parameter optimization.', tic='opt')
        log(' Optimizer: %s' % self.optimizer)
        log(' Optimizer kwargs: %s' % self.optimizer_kwargs)
        x0 = model.vector.copy()
        try:
            if self.lossprime:
                self.minimize(fun=model.get_loss,
                              x0=x0,
                              jac=model.get_lossprime,
                              method=self.optimizer,
                              options=self.optimizer_kwargs)
            else:
                if self.optimizer == 'basinhopping':
                    self.method(model.get_loss, x0, **self.optimizer_kwargs)
                else:
                    self.minimize(fun=model.get_loss,
                                  x0=x0,
                                  jac=self.optimizer,
                                  options=self.optimizer_kwargs)
        except ConvergenceOccurred:
            log('...optimization successful.', toc='opt')
            return True
        else:
            log('...optimization unsuccessful.', toc='opt')
            if self.lossprime:
                max_lossprime = \
                    max(abs(max(model.lossfunction.dloss_dparameters)),
                        abs(min(model.lossfunction.dloss_dparameters)))
                log('...maximum absolute value of loss prime: %.3e'
                    % max_lossprime)
            return False
